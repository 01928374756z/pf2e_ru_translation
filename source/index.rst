.. Pathfinder 2 Core Rulebook documentation master file, created by
   sphinx-quickstart on Wed Sep 18 11:06:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

| **ВАЖНЫЕ ОБНОВЛЕНИЯ | 2021-10-28**

| :ref:`Пушки и Шестерни (Guns & Gears) <Guns-and-Gears>`
| Часть книги про "Пушки", с опциями для персонажей и снаряжением (кроме осадных орудий и особого магического стрелкового оружия).

| `Telegram канал <https://t.me/PF2e_RU_channel>`_ - подробные обновления и изменения
| `Telegram чат <https://t.me/PF2e_RU>`_ - для обратной связи, обсуждения правил и т.п.


Pathfinder 2E
======================================================

.. toctree::
   :maxdepth: 2
   
   introduction
   ancestries_and_backgrounds/index
   classes/index
   skills
   feats
   equipment/index
   spells/index
   lost_omens/index
   playing_the_game
   game_mastering/index
   crafting_and_treasure/index
   creatures/index
   appendix/index
   licenses
