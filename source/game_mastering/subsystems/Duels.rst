.. include:: /helpers/roles.rst

.. _GMG--Subsystem--Duels:

============================================================================================================
Дуэли (`Duels <https://2e.aonprd.com/Rules.aspx?ID=1246>`_)
============================================================================================================

.. epigraph::

	*Иногда конфликты становятся личными.
	Вызову противостоит не вся группа, а только один персонаж борется с мастерством противника.
	Во многих обществах дуэли считаются разумным способом разрешения индивидуальных разногласий, хотя другие считают такую практику, особенно более смертоносные разновидности, диким оскорблением закона и порядка.
	Дуэли могут проводиться в разных формах, и в этом разделе вы найдете правила для их проведения.*

-----------------------------------------------------------------------------

**Источник**: Gamemastery Guide pg. 166


.. _Duels--Setting-Up-a-Duel:

Подготовка к дуэли (`Setting Up a Duel <https://2e.aonprd.com/Rules.aspx?ID=1247>`_)
--------------------------------------------------------------------------------------------------------

**Источник**: Gamemastery Guide pg. 166

Участники должны добровольно согласиться на дуэль и соблюдать ее правила.
Если один из дуэлянтов нарушает правила дуэли (и, что еще важнее, его застают за этим), он проигрывает, получая все наказания, оговоренные при принятии вызова.
Вот некоторые примеры дуэльных правил.


.. _Duels--Compete-Alone:

Одиночное соревнование (`Compete Alone <https://2e.aonprd.com/Rules.aspx?ID=1248>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 166

Обычно каждый участник соревнуется в одиночку и не может получать помощь извне.
Однако в некоторых поединках пары бойцов выступают друг против друга (либо все вместе, либо в составе парных команд).

.. _Duels--Limited-Tools:

Ограниченные средства (`Limited Tools <https://2e.aonprd.com/Rules.aspx?ID=1249>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 166

Перед началом дуэли участники договариваются о средствах, включая оружие и магические предметы.
Большинство боевых дуэлей, не связанных с магией, ограничивают участников оружием ближнего боя и запрещают использование яда.
В некоторых дуэлях запрещено использовать древковое оружие и другое :w_reach:`длинное` оружие.
В дуэли заклинателей можно договориться о примерно равном количестве магических предметов и ограничении по силе этих предметов (в игровых терминах - уровнях).
В некоторых дуэлях заклинателей могут быть запрещены определенные типы заклинаний, такие как :t_summoned:`призыв` или :t_necromancy:`некромантия`.

.. _Duels--Duration:

Продолжительность (`Duration <https://2e.aonprd.com/Rules.aspx?ID=1250>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 166

Боевые дуэли обычно длятся либо до первой крови (удар и нанесение урона), либо до нокаута одного из дуэлянтов.
В большинстве дуэлей участник может сдаться, то есть уступить победу своему оппоненту, хотя иногда это может снизить его социальное положение.
Дуэли талантов обычно включают несколько равных ходов, в которых дуэлянты демонстрируют свои способности.

.. _Duels--Adjudication:

Судейство (`Adjudication <https://2e.aonprd.com/Rules.aspx?ID=1251>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 166

За большинством дуэлей наблюдает третья сторона, которая следит за тем, чтобы дуэлянты не нарушили правила дуэли - случайно или жульничая.
Там, где дуэли законны, это, как правило, констебль или магистрат.
В других местах в качестве судьи выступает клирик или другая уважаемая личность.
Обычно Мастер играет роль судьи.




.. _Duels--Combat:

Боевые дуэли (`Combat Duels <https://2e.aonprd.com/Rules.aspx?ID=1252>`_)
--------------------------------------------------------------------------------------------------------

**Источник**: Gamemastery Guide pg. 166

В отличие от других подсистем, описанных в этой главе, боевая дуэль действует почти так же, как и обычное боевое столкновение, за некоторыми исключениями.
Эти правила требуют исключительной сосредоточенности двух дуэлянтов и стороннего судьи, и поэтому недоступны в обычном бою.


.. _Duels--Combat--Initiative-Actions:

Инициатива и дуэльные действия (`Initiative and Dueling Actions (combat) <https://2e.aonprd.com/Rules.aspx?ID=1253>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 167

В отличие от обычного боя, дуэлянты кидают проверку инициативы в начале каждого боевого раунда.
Каждый раунд участник поединка может при броске инициативы выбрать использование Обмана, Запугивания или Восприятия.
Поскольку дуэлянт может действовать дважды подряд, то продолжительности, которые длятся до начала следующего хода дуэлянта, могут действовать странно.
Дуэлянт, действующий вторым, может на свой выбор избегать таких умений в этот ход, или может выбрать действовать вторым, если он выиграет инициативу в следующем раунде.

Если дуэлянт как минимум обучен выбранной инициативе, то в этом раунде он получает соответствующую дуэльную реакцию:

* :ref:`action--GMG--Bullying-Press` для Запугивания
* :ref:`action--GMG--Deceptive-Sidestep` для Обмана
* :ref:`action--GMG--Sense-Weakness` для Восприятия

Ни один из дуэлянтов не знает, какой тип броска инициативы был использован другим - неожиданность и использование дуэльных действий являются частью стратегий поединка.
Фамильяры и компаньоны, даже если они допущены к дуэли, равно как и посторонние, не могут использовать эти действия.

.. rst-class:: description
.. _action--GMG--Bullying-Press:

Агрессивный натиск (`Bullying Press <https://2e.aonprd.com/Actions.aspx?ID=471>`_) |д-р|
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- размах

**Триггер**: Вы совершаете :ref:`action--Strike` ближнего боя по противнику, но еще не совершили бросок

**Требования**: Вы в дуэле, обучены Запугиванию и кинули в этом раунде инициативу с помощью Запугивания

**Источник**: Gamemastery Guide pg. 167

----------

При вашем попадании, противник становится :c_frightened:`напуган 1`.
Если, когда было применено это умение, ваш оппонент использовал для инициативы Восприятие, то вместо этого он становится :c_frightened:`напуган 2`.

.. rst-class:: description
.. _action--GMG--Deceptive-Sidestep:

Обманное уклонение (`Deceptive Sidestep <https://2e.aonprd.com/Actions.aspx?ID=472>`_) |д-р|
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- неудача

**Триггер**: Оппонент попадает по вам :ref:`Ударом (Strike) <action--Strike>` ближнего боя, но не критически

**Требования**: Вы в дуэле, обучены Обману и кинули в этом раунде инициативу с помощью Обмана

**Источник**: Gamemastery Guide pg. 167

----------

Вы подпускаете противника к себе и отступаете в последний момент.
Спровоцировавший противник должен снова сделать бросок и использовать второй результат.
Если, когда было применено это умение, ваш оппонент использовал для инициативы Запугивание, то он еще получает штраф обстоятельства -2 для второго броска атаки.

.. rst-class:: description
.. _action--GMG--Sense-Weakness:

Почувствовать слабость (`Sense Weakness <https://2e.aonprd.com/Actions.aspx?ID=473>`_) |д-р|
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

**Триггер**: Вы совершаете :ref:`action--Strike` ближнего боя по противнику, но еще не совершили бросок

**Требования**: Вы в дуэле, обучены Восприятию и кинули в этом раунде инициативу с помощью Восприятия

**Источник**: Gamemastery Guide pg. 167

----------

Вы выбираете для атаки точный момент, что дает вам преимущество.
Ваш противник :c_flat_footed:`застигнут врасплох` против этой атаки.
Если, когда было применено это умение, ваш оппонент использовал для инициативы Обман, то вместо этого он :c_flat_footed:`застигнут врасплох` до начала вашего следующего хода.



.. _Duels--Combat--Ending:

Завершение дуэли (`Ending the Duel (combat) <https://2e.aonprd.com/Rules.aspx?ID=1254>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 167

Дуэль заканчивается, когда судья подтверждает условие победы, когда судья определяет, что один из дуэлянтов сжульничал, или когда один из дуэлянтов сдается.
Обратите внимание, что если любой из участников поединка пытается продолжить дуэль после ее окончания, то участники должны кинуть инициативу и продолжить обычный бой.




.. _Duels--Spellcasting:

Заклинательские дуэли (`Spellcasting Duels <https://2e.aonprd.com/Rules.aspx?ID=1255>`_)
--------------------------------------------------------------------------------------------------------

**Источник**: Gamemastery Guide pg. 167

Как и боевые дуэли, дуэли заклинателей происходят в режиме столкновения, но их правила недоступны во время обычного боя.
Как правило, такие дуэли более организованы, чем боевые.
Многие дуэли заклинателей запрещают любые виды боя, кроме заклинаний.
Как правило, дуэлянты по очереди творят заклинания в течение одного хода, давая сопернику шанс парировать заклинания, если он сможет это сделать.


.. _Duels--Spellcasting--Initiative-Actions:

Инициатива и дуэльные действия (`Initiative and Dueling Actions (spellcasting) <https://2e.aonprd.com/Rules.aspx?ID=1256>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 167

В большинстве случаев, каждый дуэлянт кидает инициативу как обычно и в отличие от боевой дуэли, действует в порядке инициативы на протяжении всей дуэли.
Каждый дуэлянт может вместо Восприятия кидать проверку Арканы, Природы, Оккультизма или Религии.
Если они обучены этому навыку, то получают фокус магического обычая этого навыка, что позволяет им сосредоточиться на определенной магии, соответствующей проверке, которую они сделали для своего броска инициативы.
Еще они получают реакцию :ref:`action--GMG--Dueling-Counter`, которая позволяет дуэлянту парировать заклинание противника, если его фокус обычая совпадает с обычаем этого заклинания.
Также, они получают действие :ref:`action--GMG--Change-Tradition-Focus`, которое меняет фокус обычая дуэлянта на другой.
Фамильяры и компаньоны, даже если они допущены к дуэли, равно как и посторонние, не могут использовать эти действия.

При смешивании дуэли заклинателей и боевой дуэли используйте правила инициативы для боевой дуэли, но разрешите дуэлянтам для проверок инициативы кидать Аркану, Природу, Оккультизм или Религию.
Они по-прежнему получают реакцию :ref:`action--GMG--Dueling-Counter` и действие :ref:`action--GMG--Change-Tradition-Focus`, хотя "Изменить фокус обычая" менее полезно в боевых дуэлях.

.. rst-class:: description
.. _action--GMG--Dueling-Counter:

Дуэльное контрзаклинание (`Dueling Counter <https://2e.aonprd.com/Actions.aspx?ID=474>`_) |д-р|
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- магический

**Триггер**: Ваш оппонент выполняет :ref:`action--Cast-a-Spell` того же обычая, что и ваш фокус обычая

**Требования**: Вы в дуэле, и имеете фокус обычая

**Источник**: Gamemastery Guide pg. 167

----------

Израсходуйте подготовленное заклинание или слот.
После этого вы пытаетесь :ref:`противодействовать <ch9--Counteracting>` спровоцировавшему заклинанию с помощью потраченного слота.

.. rst-class:: description
.. _action--GMG--Change-Tradition-Focus:

Изменить фокус обычая (`Change Tradition Focus <https://2e.aonprd.com/Actions.aspx?ID=475>`_) |д-1|
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

**Требования**: Вы в дуэле и обучены навыку на обычая которого вы меняете свой фокус (Аркана для :t_arcana:`арканного`, Оккультизм для :t_occult:`оккультного`, Природа для :t_primal:`природного` или Религия для :t_divine:`сакрального`)

**Источник**: Gamemastery Guide pg. 167

----------

Вы меняете свой фокус обычая на другой магический обычай.



.. _Duels--Spellcasting--Ending:

Завершение дуэли (`Ending the Duel (spellcasting) <https://2e.aonprd.com/Rules.aspx?ID=1257>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Gamemastery Guide pg. 167

Как и боевая дуэль, дуэль заклинателей заканчивается, когда судья подтверждает условие победы, когда судья определяет, что один из дуэлянтов сжульничал, или когда один из дуэлянтов сдается.
Как и в случае с боевой дуэлью, если кто-то из участников пытается продолжить поединок после его окончания, участники должны кинуть инициативу и продолжить обычный бой.





.. include:: /helpers/actions.rst