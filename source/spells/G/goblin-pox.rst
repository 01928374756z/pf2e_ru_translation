.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--g--Goblin-Pox:

Гоблинская оспа (`Goblin Pox <https://2e.aonprd.com/Spells.aspx?ID=139>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- некромантия
- болезнь

**Обычай**: арканный, природный

**Сотворение**: |д-2| жестовый, словесный

**Дистанция**: касание

**Цели**: 1 существо

**Спасбросок**: Стойкость

**Божества**:
:doc:`/lost_omens/Deity/Core/Urgathoa`,
:doc:`/lost_omens/Deity/Other/Ghlaunder`,
:doc:`/lost_omens/Deity/Horseman/Apollyon`

----------

Ваше прикосновение поражает цель гоблинской оспой - раздражающей аллергической сыпью (см. описание далее).
Цель должна совершить спасбросок Стойкости.

| **Критический успех**: Цель невредима
| **Успех**: Цель получает состояние :c_sickened:`тошнота 1`
| **Провал**: Цель заражается гоблинской оспой 1-й стадии
| **Критический провал**: Цель заражается гоблинской оспой 2-й стадии

.. versionchanged:: /errata-r1
	Убран признак "атака".

----------

**Гоблинская оспа** (болезнь):

| **Уровень**: 1; Гоблины и псы гоблинов иммунны
| **Стадия 1**: Состояние :c_sickened:`тошнота 1` (1 раунд)
| **Стадия 2**: Состояния :c_sickened:`тошнота 1` и :c_slowed:`замедлен 1` (1 раунд)
| **Стадия 3**: Состояние :c_sickened:`тошнота 1` и существо не может снизить значение :c_sickened:`тошноты` ниже 1 (1 день)





.. include:: /helpers/actions.rst