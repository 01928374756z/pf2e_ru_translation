.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Sanctified-Ground:

Освященная земля (`Sanctified Ground <https://2e.aonprd.com/Spells.aspx?ID=265>`_) / Закл. 3
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение
- освящение

**Обычай**: сакральный

**Сотворение**: 1 минута (жестовый, словесный, материальный)

**Стоимость**: 1 бутылек :ref:`item--Holy-Water`

**Область**: 30-футовый взрыв исходящий от вас

**Продолжительность**: 24 часа

----------

Вы освящаете область, окропляя ее *святой водой* и защищая от врагов.
Выберите аберраций, небожителей, драконов, бесов, наблюдателей, или нежить.
Все существа в области получают бонус состояния +1 к КБ, броскам атак, броскам урона, и спасброскам против выбранных существ.





.. include:: /helpers/actions.rst