.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
СЕМЕЙСТВО (`FAMILY <https>`_)
***********************************************************************************************************

**Источник**: Bestiary # pg. ##

.. sidebar:: |rules|
.. sidebar:: |lore|
.. sidebar:: |locations|
.. sidebar:: |creatures|
.. sidebar:: |treasure|

   .. rubric:: ЗАГОЛОВОК (HEADER)

   ТЕКСТ_САЙДБАРА

ОПИСАНИЕ



.. toctree::
   :glob:
   :maxdepth: 3
   
   *



.. rst-class:: h3
.. rubric:: ЗАГОЛОВОК (HEADER)

|rules|
|lore|
|locations|
|creatures|
|treasure|

**Источник**: 

ТЕКСТ_ДОП_ИНФОРМАЦИИ





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst