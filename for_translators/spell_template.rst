.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--TITLE:

НАЗВАНИЕ (`TITLE <https>`_) / Закл. #
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :rare:`редкий`
- :uncommon:`необычный`
- 

**Обычай**: арканный, сакральный, оккультный, природный

**Сотворение**: |д-#| жестовый, словесный, материальный

**Дистанция**: 

**Цели**: 

**Область**: 

**Спасбросок**: 

**Продолжительность**: 

**Стоимость**: 

**Триггер**: 

**Требования**: 

**Божества**:
:doc:`/lost_omens/Deity/CATEGORY/NAME`,

**Пантеоны**:
:doc:`/lost_omens/Deity/Pantheon/NAME`,

**Источник**: Secrets of Magic pg. 9999999999999999999999999999999999999999999

----------

ОПИСАНИЕ

| **Критический успех**: 
| **Успех**: 
| **Провал**: 
| **Критический провал**: 

----------

**Усиление ()**: 





.. include:: /helpers/actions.rst